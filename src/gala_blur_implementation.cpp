#include <nan.h>
extern "C" {
    #include "BlurWindow.h"
}

using namespace v8;


NAN_METHOD(BlurWindow) {
    String::Utf8Value str(info[0]);
    const char *parameter = (const char*)(*str);
    uint32_t winid = atoi(parameter);
    if (winid != 0) {
        blurWindow(winid);
    }
}

NAN_MODULE_INIT(RegisterModule) {
    NAN_EXPORT(target, BlurWindow);
}

NODE_MODULE(gala_blur_implementation, RegisterModule);