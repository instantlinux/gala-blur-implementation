# gala-blur-implementation
> Implementation of the new blur-feature in the Gala WM

## Dependencies

* NodeJS
* GTK-3.0
* Gala

## Building

At first you have to install the dependencies:

```sh
npm install
```

Then you can build the module:

```sh
npm build .
```

## Testing and Usage

```js
var gala_blur = require('./build/Release/gala-blur-implementation');

gala_blur.BlurWindow("<Window XID>");
```

## Release History

* Initial Commit

## Meta

Hannes Schulze ([guidedlinux.org](https://www.guidedlinux.org/)) projects@guidedlinux.org

Distributed under the GPL-3.0 license. See ``LICENSE`` for more information.

[https://bitbucket.org/guidedlinux/](https://bitbucket.org/guidedlinux/)

## Contributing

1. Fork it (<https://bitbucket.org/instantlinux/gala-blur-implementation>)
1. Create your feature branch (`git checkout -b feature/fooBar`)
1. Commit your changes (`git commit -am 'Add some fooBar'`)
1. Push to the branch (`git push origin feature/fooBar`)
1. Create a new Pull Request
