{
    "variables": {
        "pkg-config": "pkg-config"
    },
    "targets": [
        {
            "target_name": "gala-blur-implementation",
            "sources": [ "src/gala_blur_implementation.cpp",
                "src/BlurWindow.c" ],
            "include_dirs": [
                "<!(node -e \"require('nan')\")"
            ],
            "cflags": [
                '<!@(<(pkg-config) --cflags gdk-x11-3.0 gtk+-3.0)'
            ],
            "ldflags": [
                '<!@(<(pkg-config) --libs-only-L --libs-only-other gdk-x11-3.0 gtk+-3.0)'
            ],
            "libraries": [
                '<!@(<(pkg-config) --libs-only-l gdk-x11-3.0 gtk+-3.0)'
            ]
        }
    ]
}